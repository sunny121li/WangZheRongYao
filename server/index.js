const express = require('express')
const app = express()

app.use(express.json())
app.use(require('cors')()) //引用跨域模块并直接调用

require('./plugins/db')(app)
require('./routes/admin')(app)

//全局设置返回登陆时候需要用到的密钥(自定义)
app.set('secret','i6gdh5a2u5i9sb51das4iud07hsi')

//静态文件托管后面的'/'不能写成 './' 
app.use('/admin', express.static(__dirname + '/public/admin'))
app.use('/', express.static(__dirname + '/public/web'))
app.use('/uploads', express.static(__dirname + '/uploads'))


app.get('/', async (request, response) => {
    response.send('服务器启动')
})

app.listen(3000, () => {
    console.log("http://localhost:3000/");
})